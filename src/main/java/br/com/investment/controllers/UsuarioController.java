package br.com.investment.controllers;

import br.com.investment.DTOs.CadastroDeUsuarioDTO;
import br.com.investment.models.Usuario;
import br.com.investment.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    public Usuario salvarUsuario(@RequestBody @Valid CadastroDeUsuarioDTO cadastroDeUsuarioDTO){
        return usuarioService.salvarUsuario(cadastroDeUsuarioDTO);
    }
}
