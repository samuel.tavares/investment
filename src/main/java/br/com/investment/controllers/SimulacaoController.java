package br.com.investment.controllers;

import br.com.investment.DTOs.SimulacaoDTO;
import br.com.investment.models.Simulacao;
import br.com.investment.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    public Iterable<Simulacao> listarSimulacoes(){
        return simulacaoService.listarSimulacoes();
    }
}
