package br.com.investment.controllers;

import br.com.investment.DTOs.CadastroDeProdutoDTO;
import br.com.investment.DTOs.SimulacaoDTO;
import br.com.investment.DTOs.SimulacaoResponseDTO;
import br.com.investment.models.Produto;
import br.com.investment.models.Simulacao;
import br.com.investment.services.ProdutoService;
import br.com.investment.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto criarProdutoDeInvestimento(@RequestBody @Valid CadastroDeProdutoDTO cadastroDeProdutoDTO){
        Produto produtoResponse = produtoService.criarProdutoDeInvestimento(cadastroDeProdutoDTO);
        return  produtoResponse;
    }

    @GetMapping
    public Iterable<Produto> listarProdutosDeInvestimento(){
        return produtoService.listarProdutosDeInvestimento();
    }

    @PostMapping("/{id}/simulacao")
    public SimulacaoResponseDTO simularinvestimento(@RequestBody @Valid SimulacaoDTO simulacaoDTO, @PathVariable(name = "id")int id){
        SimulacaoResponseDTO simulacaoResponseDTO = produtoService.simularValorizacaoDeInvestimento(simulacaoDTO, id);
        return simulacaoResponseDTO;
    }
}
