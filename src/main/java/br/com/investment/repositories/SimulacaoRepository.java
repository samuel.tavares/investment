package br.com.investment.repositories;

import br.com.investment.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer> {
}
