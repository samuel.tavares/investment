package br.com.investment.repositories;

import br.com.investment.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

    Usuario findByCpf(String cpf);
}
