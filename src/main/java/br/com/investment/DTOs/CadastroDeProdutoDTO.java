package br.com.investment.DTOs;

import br.com.investment.models.Produto;

import javax.persistence.Column;
import javax.validation.constraints.*;

public class CadastroDeProdutoDTO {

    @NotNull(message = "Nome não pode ser nulo")
    @NotBlank(message = "Nome não pode ser vazio")
    @Size(min = 3, message = "Minimo 3 caracteres")
    private String nome;

    @DecimalMin("0.01")
    private double rentabilidade;

    public CadastroDeProdutoDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRentabilidade() {
        return rentabilidade;
    }

    public void setRentabilidade(double rentabilidade) {
        this.rentabilidade = rentabilidade;
    }

    public Produto converterParaProduto(){
        Produto produto = new Produto();
        produto.setNome(this.nome);
        produto.setRentabilidade(this.rentabilidade);
        return produto;
    }
}
