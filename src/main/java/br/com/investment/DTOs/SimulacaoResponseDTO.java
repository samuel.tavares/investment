package br.com.investment.DTOs;

public class SimulacaoResponseDTO {

    private double rendimentoPorMes;
    private double montante;

    public SimulacaoResponseDTO() {
    }

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }
}

