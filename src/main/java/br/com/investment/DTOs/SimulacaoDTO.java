package br.com.investment.DTOs;

import br.com.investment.models.Simulacao;

import javax.validation.constraints.*;

public class SimulacaoDTO {

    @NotBlank(message = "Nome não pode ser vazio")
    @NotNull(message = "Nome não pode ser nulo")
    private String nomeInteressado;

    @Email(message = "Email inválido")
    private String email;

    @DecimalMin("0.01")
    private Double valorAplicado;

    @DecimalMin("1")
    private int quantidadeDeMeses;

    public SimulacaoDTO() {
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(Double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeDeMeses() {
        return quantidadeDeMeses;
    }

    public void setQuantidadeDeMeses(int quantidadeDeMeses) {
        this.quantidadeDeMeses = quantidadeDeMeses;
    }

    public Simulacao converterDTOParaSimulacao(){
        Simulacao simulacao = new Simulacao();

        simulacao.setNomeInteressado(this.nomeInteressado);
        simulacao.setEmail(this.email);
        simulacao.setValorAplicado(this.valorAplicado);
        simulacao.setQuantidadeDeMeses(this.quantidadeDeMeses);
        return simulacao;
    }
}
