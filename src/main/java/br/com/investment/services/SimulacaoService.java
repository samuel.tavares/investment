package br.com.investment.services;

import br.com.investment.DTOs.SimulacaoDTO;
import br.com.investment.DTOs.SimulacaoResponseDTO;
import br.com.investment.models.Produto;
import br.com.investment.models.Simulacao;
import br.com.investment.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    public Simulacao salvarSimulacao(Simulacao simulacao){
        return simulacaoRepository.save(simulacao);
    }

    public Iterable<Simulacao> listarSimulacoes(){
        return simulacaoRepository.findAll();
    }

}
