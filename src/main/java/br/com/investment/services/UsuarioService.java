package br.com.investment.services;

import br.com.investment.DTOs.CadastroDeUsuarioDTO;
import br.com.investment.auth.AuthUsuario;
import br.com.investment.models.Usuario;
import br.com.investment.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;
    private BCryptPasswordEncoder encoder;

    public Usuario salvarUsuario(CadastroDeUsuarioDTO usuarioDTO){
        return usuarioRepository.save(usuarioDTO.converterDTOParaUsuario());
    }

    @Override
    public UserDetails loadUserByUsername(String cpf) throws UsernameNotFoundException {
        Usuario usuario = usuarioRepository.findByCpf(cpf);
        if(usuario == null){
            throw new UsernameNotFoundException("Usuário não cadastrado");
        }
        AuthUsuario authUsuario = new AuthUsuario(usuario.getId(), usuario.getCpf(), usuario.getSenha());
        return authUsuario;
    }
}
