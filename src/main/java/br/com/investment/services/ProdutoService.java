package br.com.investment.services;

import br.com.investment.DTOs.CadastroDeProdutoDTO;
import br.com.investment.DTOs.SimulacaoDTO;
import br.com.investment.DTOs.SimulacaoResponseDTO;
import br.com.investment.models.Produto;
import br.com.investment.models.Simulacao;
import br.com.investment.repositories.ProdutoRepository;
import br.com.investment.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private SimulacaoService simulacaoService;

    public Produto criarProdutoDeInvestimento(CadastroDeProdutoDTO cadastroDeProdutoDTO){
        Produto produtoResponse = produtoRepository.save(cadastroDeProdutoDTO.converterParaProduto());
        return produtoResponse;
    }

    public Iterable<Produto> listarProdutosDeInvestimento(){
        return produtoRepository.findAll();
    }

    public SimulacaoResponseDTO simularValorizacaoDeInvestimento(SimulacaoDTO simulacaoDTO, int idInvestimento){

        //verifica se investimento selecionado existe
        Optional<Produto> produtoSimulado = produtoRepository.findById(idInvestimento);
        if(produtoSimulado == null) {
            throw new RuntimeException("Investimento não encontrado");
        }

        //Salvar simulaçao no banco
        Simulacao simulacao = simulacaoDTO.converterDTOParaSimulacao();
        simulacao.setProduto(produtoSimulado.get());
        simulacaoService.salvarSimulacao(simulacao);

        //manipula objetos e calcula simulação
        SimulacaoResponseDTO simulacaoResponseDTO = new SimulacaoResponseDTO();

        double total = simulacao.getValorAplicado();
        double taxa = produtoSimulado.get().getRentabilidade();

        for(int i=0; i<simulacao.getQuantidadeDeMeses(); i++){
            total = total + total*taxa;
        }
        simulacaoResponseDTO.setMontante(total);
        simulacaoResponseDTO.setRendimentoPorMes(taxa);


        return simulacaoResponseDTO;
    }
}
